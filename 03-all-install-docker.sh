sudo apt install -y curl gnupg2 software-properties-common apt-transport-https ca-certificates

sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | \
     sudo gpg --dearmour -o /etc/apt/trusted.gpg.d/docker.gpg

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

sudo apt update

sudo apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin

# Configure containerd so that it starts using systemd as cgroup.

containerd config default | sudo tee /etc/containerd/config.toml >/dev/null 2>&1

sudo sed -i 's/SystemdCgroup \= false/SystemdCgroup \= true/g' \
     /etc/containerd/config.toml

# Restart containerd services

sudo systemctl restart containerd

sudo systemctl enable containerd

echo ############################
echo If any errors related to container runtime, try the following
echo ""
echo sudo rm /etc/containerd/config.toml
echo ""
echo sudo rm /etc/containerd/config.toml
echo ############################

sudo usermod -aG docker $USER

echo ############################
echo User $USER added to docker group
echo Logout and login to apply
echo ############################
