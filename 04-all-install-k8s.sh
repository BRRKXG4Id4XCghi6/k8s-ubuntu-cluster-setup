curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

LATEST_DIST=xenial
sudo apt-add-repository "deb http://apt.kubernetes.io/ kubernetes-${LATEST_DIST} main"

sudo apt update

sudo apt install -y kubelet kubeadm kubectl

sudo apt-mark hold kubelet kubeadm kubectl

