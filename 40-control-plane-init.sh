CONTROLLER_HOST_IP=10.10.0.10
POD_NETWORK_CIDR=10.50.50.0/16

sudo kubeadm init \
   --pod-network-cidr=${POD_CONTROLLER_CIDR} \
   --apiserver-advertise-address=${CONTROLLER_HOST_IP}

mkdir -p $HOME/.kube

sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config

sudo chown $(id -u):$(id -g) $HOME/.kube/config
