SECRET_OBJ_NAME=private-reg-cred

kubectl create secret generic \
  ${SECRET_OBJ_NAME} \
  --from-file=.dockerconfigjson=/home/${USER}/.docker/config.json \
  --type=kubernetes.io/dockerconfigjson


