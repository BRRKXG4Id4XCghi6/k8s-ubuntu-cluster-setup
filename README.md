**Always update first**

`sudo apt update && sudo apt upgrade -y`

----

## On ALL hosts

Basic minimal steps

```
01-all-disable-swap.sh

02-all-install-kernel-modules.sh

03-all-install-docker.sh

04-all-install-k8s.sh
```

## On the Control Plane host

On Control Plane, initialise the cluster and the network fabric.
```
40-control-plane-init.sh

41-control-plane-install-cni.sh
```

## Private container repository

If a Gitea container repository is available at `gitea.lan`, copy request certs and set up the `ImagePullSecret`.
```
90-all-gitea-certs.sh <SCP PATH>

91-control-plane-gitea-secret.sh
```
