if [ ! -f $HOME/.kube/config ] 
then
   echo Control plane not yet initialized.
   exit	
fi


curl https://projectcalico.docs.tigera.io/manifests/calico.yaml -O

kubectl apply -f calico.yaml

